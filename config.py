#!/usr/bin/python3
#--------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# config.py
#
# Imported by other scripts to provide
# personalised information.
#
# ENSURE YOU SET YOUR OWN VALUES
#
# Author : Matt Hawkins
# Date   : 07/12/2018
# Source : https://bitbucket.org/MattHawkinsUK/rpispy-alexa-energenie-hub/
#
# Additional details here:
# https://www.raspberrypi-spy.co.uk/
#
#--------------------------------------

# Edit the name of your system
mytitle="Jeeves"

# Edit these two variables to match your Pushover details
pushuser="1234"
pushtoken="1234"
pushmessage=mytitle+" has rebooted"

# Define friendly names for 4 sockets
socketnames = ['lamp',
               'reading light',
               'lava lamp',
               'christmas tree'
              ]

# Define scenes to set state for all 4 sockets
# Names should only contain letters a-z
scenes =	{
  "lamps": [True,False,True,False],
  "tv": [False,False,True,False],
  "christmas": [True,False,True,True]
}
