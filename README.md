# RPiSpy Alexa Energenie Hub

Repository for my Alexa Energenie hub project.

## Files

* alexa_energenie_hub.py
** Main script that creates Flask webserver and responds to requests from the Alexa Skill.
* pushover_boot.py
** Script to send Pushover notification on boot
* templates.yaml
** Template for some responses that are passed to the Alexa Skill.
* config.py
** Configuration and custom values that the user can change.
* energenie_pair.py
** Script to pair the Energenie sockets with the Pi-Mote

## Installation

Start from the home directory :
```
cd /home/pi
```
Then to download files directly to the Pi use:
```
git clone https://MattHawkinsUK@bitbucket.org/MattHawkinsUK/rpispy-alexa-energenie-hub.git
```
Then rename directory :
```
mv rpispy-alexa-energenie-hub alexa
```

All the files should now be /home/pi/alexa.

Edit config.py to add Pushover API and User tokens.

## Pushover
If you want [https://pushover.net/ Pushover] notifications when the Pi reboots then you need a Pushover account. Enter keys into config.py.
* pushtoken is the "API Token/Key" found in your Pushover application.
* pushuser is the "Your User Key" shown on the Pushover dashboard.

## Cron
In order to start the main script and send Pushover notifications some Cron settings must be configured :
```
sudo crontab -e
```
then add :
```
@reboot /home/pi/alexa/alexa_energenie_hub.py > /home/pi/alexa/cronlog 2>&1 &
@reboot /home/pi/alexa/pushover_boot.py > /home/pi/alexa/cronlog 2>&1 &
* 1 */2 * * reboot
```
The last line reboots the Pi at 1am every two days.