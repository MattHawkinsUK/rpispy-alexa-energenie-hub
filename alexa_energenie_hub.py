#!/usr/bin/python3
#-----------------------------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# alexa_energenie_hub.py
#
# This script uses Flask to create basic webservice
# that can respond to an Alexa Skill.
#
# The Flask-ask library is used to read/write requests
# from the Alexa server. Gpiozero is used to control
# the Energenie Pi-Mote add-on board.
#
# Author : Matt Hawkins
# Date   : 07/01/2019
# Source : https://bitbucket.org/MattHawkinsUK/rpispy-alexa-energenie-hub/
#
# Additional details here:
# https://www.raspberrypi-spy.co.uk/
#
#-----------------------------------------------------------
import os
import logging
import time
from flask import Flask
from flask import request
from flask import render_template
from flask_ask import Ask, statement
from gpiozero import Energenie

# Import settings unique to user
import config as c

logging.basicConfig(format='%(asctime)s %(message)s',filename='/home/pi/alexa/alexa_energenie_hub.log',level=logging.INFO)

logging.info('Script started')

# Create 4 objects
socket1 = Energenie(1)
socket2 = Energenie(2)
socket3 = Energenie(3)
socket4 = Energenie(4)

# Create a Flask website
app = Flask(__name__)
ask = Ask(app, '/')


def socketControl(num,state):
  """
  Takes two strings that represent a socket number and state.
  Socket number should be '0' to '4' where '0' represents all
  sockets. State should be 'on' or 'off'.
  """
  for i in range(2):
    if state=='on':
      if num=='1':
        socket1.on()
      elif num=='2':
        socket2.on()
      elif num=='3':
        socket3.on()
      elif num=='4':
        socket4.on()
      elif num=='0':
        socket1.on()
        socket2.on()
        socket3.on()
        socket4.on()
    elif state=='off':
      if num=='1':
        socket1.off()
      elif num=='2':
        socket2.off()
      elif num=='3':
        socket3.off()
      elif num=='4':
        socket4.off()
      elif num=='0':
        socket1.off()
        socket2.off()
        socket3.off()
        socket4.off()
    time.sleep(0.1)


@ask.intent('SocketControlIntent')
def socket_control(socketname,socketnumber,state):
  """
  Handles socket_control intent.
  """
  currentIntent='Socket Control'

  logging.info('%s : %s %s %s',currentIntent,socketname,socketnumber,state)

  if state!=None and socketname==None and socketnumber==None:
    # State specified but no name or number
    if state in ['on','off']:
      socketControl('0',state)
      title='All Sockets '+state
      text=render_template('sockets_all',state=state)
    else:
      title='Opps!'
      text=render_template('invalid_state',states='on or off')

  elif state!=None and socketnumber!=None:
    # State and Socket Number specified
    if state in ['on','off'] and socketnumber in ['1','2','3','4']:
      socketControl(socketnumber,state)
      title='Socket '+state
      text=render_template('socket_state',socket=socketnumber,state=state)
    else:
      title='Opps!'
      text=render_template('opps_sorry')

  elif state!=None and socketname!=None:
    # State and Socket Name specified
    socketname=socketname.lower()
    if socketname in c.socketnames and state in ['on','off']:
      index=c.socketnames.index(socketname)+1
      socketControl(str(index),state)
      socketname=socketname.capitalize()
      title=socketname+' '+state
      text=render_template('socket_name',socket=socketname,state=state)
    else:
      title='Opps!'
      socketnames=",".join(c.socketnames)
      text=render_template('invalid_socket',socketnames=socketnames)

  else:
    title='Opps!'
    text=render_template('opps_sorry')

  logging.info('%s : %s',currentIntent,text)

  return statement(text).simple_card(title,text)


@ask.intent('SocketSceneIntent')
def socket_scene(sceneName):
  """
  Handles socket_scene intent.
  Incoming scene name is simplified to maximise chance it is
  matched to scenes defined in config file.
  """
  currentIntent='SocketSceneIntent'
  title='Scene Updated'

  sceneName=sceneName.replace(" ", "")
  sceneName=sceneName.replace(".", "")
  sceneName=sceneName.lower()

  logging.info('Socket Scene Intent : sceneName %s',sceneName)

  if sceneName in c.scenes:

    socketStates=c.scenes[sceneName]

    socketControl('1','on') if socketStates[0] else socketControl('1','off')
    socketControl('2','on') if socketStates[1] else socketControl('2','off')
    socketControl('3','on') if socketStates[2] else socketControl('3','off')
    socketControl('4','on') if socketStates[3] else socketControl('4','off')

    text=render_template('scene_state',scene=sceneName)

  else:
    title='Opps!'
    scenenames=",".join(c.scenes.keys())
    text=render_template('invalid_scene',scenenames=scenenames)

  logging.info('%s : %s',currentIntent,text)

  return statement(text).simple_card(title,text)


@ask.intent('AdminIntent')
def admin(command):
  """
  Handles admin intent.
  """
  currentIntent='AdminIntent'
  title='Admin Command'

  if command!=None:
    command=command.lower().replace(" ","")
    if command=='reboot' or command=='restart':
      text="Rebooting"
      os.system('sleep 10 && sudo reboot &')
    if command=='shutdown':
      text="Shutting down"
      os.system('sleep 10 && sudo shutdown -h now &')
    if command=='test' or command=='selftest':
      socketControl('0','off')
      socketControl('1','on')
      socketControl('2','on')
      socketControl('3','on')
      socketControl('4','on')
      time.sleep(0.3)
      socketControl('0','off')
      text=render_template('test',name=c.mytitle)
  else:
    text="A valid command is missing"

  logging.info('%s : %s',currentIntent,text)

  return statement(text).simple_card(title,text)


@ask.intent('AMAZON.HelpIntent')
def help():
  """
  Handles default Help intent.
  """
  currentIntent='HelpIntent'
  title="Help"

  socketnames=",".join(c.socketnames)
  text=render_template('help',socketnames=socketnames)

  logging.info('%s : %s',currentIntent,text)

  return statement(text).simple_card(title,text)

if __name__ == "__main__":
  app.run(host='0.0.0.0',port=80,debug=False)
