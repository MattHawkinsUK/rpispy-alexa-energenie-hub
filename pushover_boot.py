#!/usr/bin/python3
#--------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# pushover_boot.py
#
# Send pushover notification when Pi boots.
#
# Author : Matt Hawkins
# Date   : 07/12/2018
# Source : https://bitbucket.org/MattHawkinsUK/rpispy-alexa-energenie-hub/
#
# Additional details here:
# https://www.raspberrypi-spy.co.uk/
#
#--------------------------------------
import http.client, urllib, datetime, time

# Import settings unique to user
import config as c

print(datetime.datetime.now(),":",c.mytitle,"has rebooted")

time.sleep(10)

try:
  # Send request to Pushover
  conn = http.client.HTTPSConnection("api.pushover.net:443")
  conn.request("POST", "/1/messages.json",
    urllib.parse.urlencode({
        "token": c.pushtoken,     # Pushover app token
        "user": c.pushuser,       # Pushover user token
        "html": "1",              # 1 for HTML, 0 to disable
        "title": c.mytitle,       # Title of message
        "message": c.pushmessage, # Message (HTML if required)
        "url": "",                # Link to include in message
        "url_title": "",          # Text for link
        "sound": "cosmic",        # Sound played on receiving device
    }), { "Content-type": "application/x-www-form-urlencoded" })
  conn.getresponse()
except:
  print(datetime.datetime.now(),":","Failed to send Pushover notification")
else:
  print(datetime.datetime.now(),":","Pushover notification sent")